package A22_2;

import java.awt.Graphics;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author Hannes
 */
public class MyCanvas extends JPanel {

    protected List<FigurenGruppe> figs = new LinkedList<FigurenGruppe>();

    /**
     * Konstruktor
     *
     * @param fg FigurenGruppe...
     */
    public MyCanvas(FigurenGruppe... fg) {
        if (fg.length == 0) {
            return;
        }
        this.figs.addAll(Arrays.asList(fg));
    }

    /**
     * Hook der zur Darstellung unserer MyCanvas Komponente aufgerufen wird
     *
     * @param g Graphics object, dass zur Darstellung der Komponente zu nutzen
     * ist.
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int x = this.getWidth() / 2;
        int y = this.getHeight() / 2;
        for (FigurenGruppe f : this.getFiguren()) {
            f.zeichne(x, y, g);
        }
    }

    /**
     * Getter der Figuren
     *
     * @return List<FigurenGruppe>
     */
    public List<FigurenGruppe> getFiguren() {
        return figs;
    }
}
