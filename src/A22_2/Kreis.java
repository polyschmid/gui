package A22_2;

import java.awt.Color;

/**
 *
 * @author hannes
 */
public class Kreis extends Ellipse {

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param r int Radius des Kreise
     * @param Color Farbe des Objektes
     */
    public Kreis(int x, int y, int z, int r, Color FARBE) {

        super(x, y, z, r, r, FARBE);
    }
}
