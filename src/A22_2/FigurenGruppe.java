package A22_2;

import java.awt.Graphics;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author hannes
 */
public class FigurenGruppe {

    protected List<Figur> figs = new LinkedList<Figur>();
    protected int X;
    protected int Y;
    protected int DX;
    protected int DY;
    protected Random r = new Random(); // Ka muss  später wieder raus!
    protected int geschwindikeit = 1;

    /**
     * Konstruktor
     *
     * @param x int Koordinate
     * @param y int Koordinate
     */
    public FigurenGruppe(int x, int y) {
        this.X = x;
        this.Y = y;
    }

    /**
     * Konstruktor 2
     *
     * @param x int Koordinate
     * @param y int Koordinate
     * @param Figur... KA das UML ist nicht ganz verständlich
     */
    public FigurenGruppe(int x, int y, Figur... f1) { // ARRAY!!
        this.X = x;
        this.Y = y;
        this.DX = r.nextInt(10); // geschwindikeiten der  Bären als RANDOM
        this.DY = r.nextInt(10);
        if (f1.length == 0) {
            return;
        }
        this.figs.addAll(Arrays.asList(f1));
        this.sort();

    }

    /**
     * Getter für X-Koordinate
     *
     * @return int X Koordinate
     */
    public int getX() {
        return this.X;
    }

    /**
     * Getter für Y-Koordinate
     *
     * @return int Y Koordinate
     */
    public int getY() {
        return this.Y;
    }

    /**
     * Getter für DX-Vector
     *
     * @return int DX Vector
     */
    public int getDX() {
        return this.DX;
    }

    /**
     * Getter für Y-Vector
     *
     * @return int Y Vector
     */
    public int getDY() {
        return this.DY;
    }

    /**
     * Setter für X-Vector
     *
     * @param x int Setzt den x Wert.
     * @return FigurenGruppe X Vector
     */
    public FigurenGruppe setDX(int x) {
        this.DX = x;
        return this;
    }

    /**
     * Setter für Y-Vector
     *
     * @param y int Setzt den y Wert.
     * @return FigurenGruppe Y Vector
     */
    public FigurenGruppe setDY(int y) {
        this.DY = y;
        return this;
    }

    /**
     * Bewegt die FigurenGruppe in dem Canvas
     *
     * @return Figurengruppe wird in Koordinaten bewegt.
     */
    public FigurenGruppe bwewege() {
        this.X += this.DX;
        this.Y += this.DY;
        return this;
    }

    /**
     * Einer FigurenGruppe wird eine Figur hinzugefügt.
     *
     * @param a Figur...
     * @return FigurenGruppe
     */
    public FigurenGruppe add(Figur... a) {
        this.figs.addAll(Arrays.asList(a));
        return this;
    }

    /**
     * Löscht die Figuren einer FigurenGruppe
     *
     * @param a Figur...
     * @return FigurenGruppe
     */
    public FigurenGruppe remove(Figur... a) {
        this.figs.removeAll(Arrays.asList(a));
        return this;
    }

    /**
     * Sortiert die Figurengruppe in der Z-Ebene
     *
     * @return FigurenGruppe
     */
    protected FigurenGruppe sort() {
//        Figur[] xs = figs.toArray(new Figur[figs.size()]);
//        boolean unsorted = true;
//        while (unsorted) {
//            unsorted = false;
//            for (int i = 0; i < xs.length - 1; i++) {
//                if (!(xs[i].Z <= xs[i + 1].Z)) {
//                    Figur dummy = xs[i];
//                    xs[i] = xs[i + 1];
//                    xs[i + 1] = dummy;
//                    unsorted = true;
//                }
//            }
//        }
//        List<Figur> t = new LinkedList<Figur>(Arrays.asList(xs));
//        return t;

        this.figs.sort((a, b) -> a.Z >= b.Z ? 1 : -1); // ACHTUNG  LAMBDA Ausdruck
        return this;
    }

    /**
     * Zeichnet die einzelnen Figuren der Gruppe
     *
     * @param x Koordinate
     * @param y Koordinate
     * @param g Graphics
     */
    public void zeichne(int x, int y, Graphics g) {
        for (Figur f : this.figs) {
            f.zeichne(x + this.X, y - this.Y, g); //WEIL OBEN LINKS
        }
    }

}
