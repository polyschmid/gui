package A22_2;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author hannes
 */
public class RWDreieck extends FigMZLA {

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param a int Maße "a" des Objekt
     * @param b int Maße "b" des Objekt
     * @param Color Farbe des Objektes
     */
    public RWDreieck(int x, int y, int z, int a, int b, Color FARBE) {
        super(x, y, z, a, b, FARBE);

    }

    /**
     * Berechnet die Fläche des Dreiecks
     *
     * @return double in FE
     */
    public double berechneFlaeche() {
        return Math.abs((super.A * super.B) / 2);
    }

    /**
     * Zeichnet eine Ellipse in einer Component mittels eines Graphics Objekts
     *
     * @param x X-Position zu der relativ gezeichnet werden soll
     * @param y Y-Position zu der relativ gezeichnet werden soll
     * @param g Graphics object das zum Zeichnen genutzt werden soll
     */
    public void zeichne(int x, int y, Graphics g) {
        int x_draw = x + this.X;
        int y_draw = y - this.Y;

        int[] xPos = new int[3];
        xPos[0] = x_draw;
        xPos[1] = x_draw - this.A;  // Hier verändern
        xPos[2] = x_draw;

        int[] yPos = new int[3];
        yPos[0] = y_draw;
        yPos[1] = y_draw;
        yPos[2] = y_draw - this.B; // Hier verändern

        g.setColor(this.FARBE);
        g.fillPolygon(xPos, yPos, 3);
    }

}
