package A22_2;

import java.awt.Color;

/**
 *
 * @author hannes
 */
public abstract class FigMZLA extends Figur {

    protected int A;
    protected int B;
    protected String figurenTyp = this.getClass().getSimpleName();

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param a int Maße "a" des Objekt
     * @param b int Maße "b" des Objekt
     * @param Color Farbe des Objektes
     */
    public FigMZLA(int x, int y, int z, int a, int b, Color FARBE) {
        super(x, y, z, FARBE);// Ruft alle Koordinaten der Superklasse auf.
        this.A = a;
        this.B = b;

        if (figurenTyp.equals("RWDreieck")) {
            figurenTyp = "Rechtwinkliges Dreieck";
        }

    }

    /**
     * Berechnet die Fläche des jeweiligen Objekts. Abstrakt
     *
     * @return double Fläche des Objekts
     */
    public abstract double berechneFlaeche(); //Abstract also wird an unterklassen weiter vererbt

    /**
     * Gibt die Figur als String zurück.
     *
     * @return String Figur
     */
    public String toString() {

        return figurenTyp
                + " an Position (" + super.X + ", " + super.Y + ", " + super.Z + ") "
                + " mit der Fläche von "
                + berechneFlaeche()
                + " Flächeneinheiten.\n";

    }

    /**
     * Getter für Maß A
     *
     * @return int Maß
     */
    public int getA() {
        return this.A;
    }

    /**
     * Getter für Maß B
     *
     * @return int Maß
     */
    public int getB() {
        return this.B;
    }

}
