package A22_2;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author hannes
 */
public class Ellipse extends FigMZLA {

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param a int Maße "a" des Objekt
     * @param b int Maße "b" des Objekt
     * @param Color Farbe des Objektes
     */
    public Ellipse(int x, int y, int z, int a, int b, Color FARBE) {

        super(x, y, z, a, b, FARBE);
    }

    /**
     * Berechnet die Fläche des Objekts
     *
     * @return double Fläche des Objekts
     */
    public double berechneFlaeche() {
        return Math.abs(Math.PI * super.A * super.B);
    }

    /**
     * Zeichnet eine Ellipse in einer Component mittels eines Graphics Objekts
     *
     * @param x X-Position zu der relativ gezeichnet werden soll
     * @param y Y-Position zu der relativ gezeichnet werden soll
     * @param g Graphics object das zum Zeichnen genutzt werden soll
     */
    public void zeichne(int x, int y, Graphics g) {
        int x_draw = x + this.getX() - this.getA();
        int y_draw = y - this.getY() - this.getB();
        g.setColor(this.FARBE);
        g.fillOval(x_draw, y_draw, this.getA() * 2, this.getB() * 2);
    }

}
