package A22_2;

import java.awt.Color;
import java.util.List;
import java.util.Random;
import javax.swing.JFrame;

/**
 *
 * @author Hannes
 */
public class Main {

    public static void main(String[] args) {

        FigurenGruppe baer1 = new FigurenGruppe(0, 0,
                new Kreis(0, 0, 0, 100, Color.GRAY), // Gesicht
                new Kreis(0, -10, 1, 20, Color.RED), // Nase
                new Kreis(65, 65, -1, 40, Color.DARK_GRAY), // Linkes Ohr
                new Kreis(-65, 65, -1, 40, Color.DARK_GRAY), // Rechtes Ohr
                new Kreis(-25, 25, 1, 10, Color.WHITE), // Linkes Auge
                new Kreis(25, 25, 1, 10, Color.WHITE), // Rechtes Auge
                new Ellipse(0, -55, 1, 55, 15, Color.YELLOW), // Mund

                new Rechteck(-80, 90, 1, 160, 20, Color.GREEN), // Hut (Krempe, unten)
                new Rechteck(-40, 90, 1, 80, 100, Color.GREEN) // Hut (oben)
        );

        FigurenGruppe baer2 = new FigurenGruppe(0, 0,
                new Kreis(0, 0, 0, 100, Color.GRAY), // Gesicht
                new Kreis(-25, 25, 1, 10, Color.BLUE), // Linkes Auge
                new Kreis(25, 25, 1, 10, Color.BLUE), // Rechtes Auge
                new Kreis(0, -10, 1, 20, Color.RED), // Nase
                new Kreis(65, 65, -1, 40, Color.DARK_GRAY), // Linkes Ohr
                new Kreis(-65, 65, -1, 40, Color.DARK_GRAY), // Rechtes Ohr
                new RWDreieck(0, -40, 1, -70, -30, Color.YELLOW), // Mund (links)
                new RWDreieck(0, -40, 1, 70, -30, Color.YELLOW), // Mund (rechts)
                new RWDreieck(0, 90, 1, -40, 100, Color.GREEN), // Partyhut (links)
                new RWDreieck(0, 90, 1, 40, 100, Color.GREEN) // Partyhut (rechts)
        );

        FigurenGruppe alien = new FigurenGruppe(0, 0,
                new Ellipse(0, 0, 0, 60, 80, Color.GRAY), // Gesicht
                new Ellipse(-25, 25, 2, 5, 15, Color.DARK_GRAY), // Linkes Auge
                new Ellipse(25, 25, 2, 5, 15, Color.DARK_GRAY), // Rechtes Auge
                new Ellipse(-3, -10, 1, 2, 10, Color.DARK_GRAY), // Nase
                new Ellipse(3, -10, 1, 2, 10, Color.DARK_GRAY),
                //new Kreis(65, 65, -1, 40, Color.DARK_GRAY), // Linkes Ohr
                //new Kreis(-65, 65, -1, 40, Color.DARK_GRAY) // Rechtes Ohr
                //new RWDreieck(0, -40, 1, -70, -30, Color.DARK_GRAY), // Mund (links)
                new Kreis(0, -40, 1, 10, Color.WHITE) // Mund (rechts)
        //new RWDreieck(0, 90, 1, -40, 100, Color.GREEN), // Partyhut (links)
        //new RWDreieck(0, 90, 1, 40, 100, Color.GREEN) // Partyhut (rechts)
        );

        JFrame mainFrame = new JFrame("Bouncing Bears");
        mainFrame.setSize(800, 600);
        mainFrame.setDefaultCloseOperation(3);
        MyCanvas canvas = new MyCanvas(baer1, baer2, alien);

        mainFrame.add(canvas);
        mainFrame.setVisible(true);

        Random r = new Random();

        while (true) {
            try {
                Thread.sleep(10); // 10 ms schlafen
                int h = canvas.getHeight() / 2; // Hoehe des Canvas bestimmen
                int w = canvas.getWidth() / 2; // Breite des Canvas bestimmen

                for (FigurenGruppe fg : canvas.getFiguren()) {
                    
                    /*
                    Wenn FigurenGruppe gegen die Wand Stößt dann Pralle ab 
                    und ändere die Geschwindikeit.
                    
                    Dabei ist der Wert "80" abstandt zum Canvas
                    
                    Der Random Int wert ändert die geschwindigkeit
                    
                    */
                    /*
                    Links
                    */
                    if (fg.getDX() + fg.getX() + 80 >= w) {
                        fg.setDX((r.nextInt(10 - 1 + 1) + 1) * (-1));
//                        fg.setDY((r.nextInt(10 - 1 + 1) + 1) * (r.nextBoolean() ? -1 : 1))/*R oben oder unten*/;
                    }
                    
                    /*
                    Rechts
                    */
                    if (fg.getDX() + fg.getX() - 80 <= (-1) * (w)) {
                        fg.setDX((r.nextInt(10 - 1 + 1) + 1));
//                        fg.setDY((r.nextInt(10 - 1 + 1) + 1) * (r.nextBoolean() ? -1 : 1));
                    }
                    /*
                    Oben
                    */
                    if (fg.getDY() + fg.getY() + 80 >= h) {
                        fg.setDY((r.nextInt(10 - 1 + 1) + 1) * (-1));
                        fg.setDX((r.nextInt(10 - 1 + 1) + 1) * (r.nextBoolean() ? -1 : 1));
                    }
                    /*
                    Unten
                    */
                    if (fg.getDY() + fg.getY() - 80 <= (-1) * (h)) {
                        fg.setDY((r.nextInt(10 - 1 + 1) + 1));
                        fg.setDX((r.nextInt(10 - 1 + 1) + 1) * (r.nextBoolean() ? -1 : 1));
                    }

                    fg.bwewege();
                }

                canvas.repaint();
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }
}
