package A22_2;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author hannes
 */
public class Rechteck extends FigMZLA {

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param a int Maße "a" des Objekt
     * @param b int Maße "b" des Objekt
     * @param Color Farbe des Objektes
     */
    public Rechteck(int x, int y, int z, int a, int b, Color FARBE) {
        super(x, y, z, a, b, FARBE);
    }

    /**
     * Berechnet die Fläche des Rechtecks.
     *
     * @return double Fläche des Objektes
     */
    public double berechneFlaeche() {
        return Math.abs(super.A * super.B);
    }

    /**
     * Zeichnet eine Ellipse in einer Component mittels eines Graphics Objekts
     *
     * @param x X-Position zu der relativ gezeichnet werden soll 4
     * @param y Y-Position zu der relativ gezeichnet werden soll 5
     * @param g Graphics object das zum Zeichnen genutzt werden soll 6
     */
    public void zeichne(int x, int y, Graphics g) {
        int x_draw = x + this.X;
        int y_draw = y - this.Y - this.B; //
        g.setColor(this.FARBE);
        g.fillRect(x_draw, y_draw, this.A, this.B);
    }
}
