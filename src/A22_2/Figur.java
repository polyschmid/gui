package A22_2;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author hannes
 */
public class Figur /*extends FigurenGruppe*/ {

    protected int X;
    protected int Y;
    protected int Z;
    private static List<Figur> figuren = new ArrayList<Figur>();
    protected Color FARBE;

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param farbe Color aus der AWT
     */
    public Figur(int x, int y, int z, Color FARBE) {
        this.X = x;
        this.Y = y;
        this.Z = z;
        this.FARBE = FARBE;
        //figuren.add(this);
    }
    /**
     * Getter für X-Koordinate
     * @return  int X Koordinate
     */
    public int getX() {
        return this.X;
    }
    /**
     * Getter für Y-Koordinate
     * @return  int Y Koordinate
     */
    public int getY() {
        return this.Y;
    }
    /**
     * Getter für Z-Koordinate
     * @return  int Z Koordinate
     */
    public int getZ() {
        return this.Z;
    }
    /**
     * Zeichnet die Figuren
     * @param x int Koordinate
     * @param y int Koordinate
     * @param g Graphics 
     */
    public void zeichne(int x, int y, Graphics g){
        for (Figur f : this.figuren) {
            f.zeichne(x + this.X, y - this.Y, g);
        }        
    }
}
