package A22_2;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author hannes
 */
public class Quadrat extends Rechteck {

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param a int Maße "a" des Objekt
     * @param Color Farbe des Objektes
     *
     */
    public Quadrat(int x, int y, int z, int a, Color FARBE) {
        super(x, y, z, a, a, FARBE);
    }

    /**
     * Zeichnet eine Ellipse in einer Component mittels eines Graphics Objekts
     *
     * @param x X-Position zu der relativ gezeichnet werden soll
     * @param y Y-Position zu der relativ gezeichnet werden soll
     * @param g Graphics object das zum Zeichnen genutzt werden soll
     */
    public void zeichne(int x, int y, Graphics g) {
        int x_draw = x + this.X;
        int y_draw = y - this.Y;
        g.setColor(this.FARBE);
        g.fillRect(x_draw, y_draw, this.A, this.B);
    }
}
