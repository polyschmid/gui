package GameOfLife;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.Timer;

/**
 *
 * @author "Le Anh, Tu" Controler verarbeitet Mauseingaben von seiner View und
 * leitet die Ergebnisse zum Model weiter.
 *
 */
public class Controler implements MouseListener {

    private Timer timer;
    private View myViewRef;
    private Model myModelRef;
    int i = 0;

    /**
     * konstruiert ein Controlerobjekt.
     *
     * @param m das Model mit der der Controler interagieren soll.
     * @param v die View mit dem der Controler interagieren soll
     */
    public Controler(Model m, View v) {
        this.myModelRef = m;
        this.myViewRef = v;

        timer = new Timer(250, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                myModelRef.nextGeneration();
                myViewRef.update();
            }

        });

    }

    /**
     * verarbeitet die Mauseingaben seiner View.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof JGoLField) {
            JGoLField tmp = (JGoLField) e.getSource();
            if (tmp.isAlive()) {
                myModelRef.changeState(tmp.getRow(), tmp.getCol());
            } else if (!tmp.isAlive()) {
                myModelRef.changeState(tmp.getRow(), tmp.getCol());
            }
        }
        myViewRef.update();

        if (e.getSource() == myViewRef.buttons.get(0)) {
            timer.start();
        }

        if (e.getSource() == myViewRef.buttons.get(1)) {
            timer.stop();
        }

        if (e.getSource() == myViewRef.buttons.get(2)) {
            myModelRef.previousGeneration();
            myViewRef.update();
        }

        if (e.getSource() == myViewRef.buttons.get(3)) {
            myModelRef.clear();
            myViewRef.update();
            timer.stop();
        }

        if (e.getSource() == myViewRef.buttons.get(4)) {
            this.randomCells(10);
        }

    }

    /**
     * setzt zufällige Zellen der View auf lebendig.
     *
     * @param percentage eine Zahl zwischen 1 und 100. Der int-Wert bestimmt den
     * prozentualen Anteil der von der Gesamtanzahl der Zellen belebt werden
     * soll.
     */
    public void randomCells(int percentage) {
        int toFill = (myModelRef.getWidth() * myModelRef.getHeigth()) / 100 * percentage;
        int randomX = (int) ((Math.random()) * (myModelRef.getWidth() - 1));
        int randomY = (int) ((Math.random()) * (myModelRef.getHeigth() - 1));

        while (toFill > 0) {
            randomX = (int) ((Math.random()) * (myModelRef.getWidth() - 1));
            randomY = (int) ((Math.random()) * (myModelRef.getHeigth() - 1));
            if (!myModelRef.isAlive(randomX, randomY)) {
                myModelRef.changeState(randomX, randomY);
                toFill--;
            }
        }
        myModelRef.nextGeneration();
        myViewRef.update();

    }

    /**
     * wird nicht verwendet.
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    /**
     * wird nicht verwendet.
     */
    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    /**
     * wird nicht verwendet.
     */
    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    /**
     * wird nicht verwendet.
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }

}
