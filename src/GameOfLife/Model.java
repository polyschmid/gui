package GameOfLife;

import java.util.LinkedList;
import java.util.List;

public class Model {

    private int width;
    private int height;
    private boolean[][] actualField;
    private boolean[][] nextField;
    private List<boolean[][]> fieldHistory;

    /**
     * konstruiert ein Modelobjekt.
     */
    public Model() {
        fieldHistory = new LinkedList<boolean[][]>();
    }

    /**
     * konstruiert ein Modelobjekt und setzt die Feldgrößen für das Spiel fest.
     *
     * @param w Breite des Spielfeldes.
     * @param h Höhe des Spielfeldes
     */
    public Model(int w, int h) {
        this.width = w;
        this.height = h;
        actualField = new boolean[w][h];
        nextField = new boolean[w][h];
        fieldHistory = new LinkedList<boolean[][]>();
    }

    /**
     * gibt die Weite des Spielfeldes zurück.
     *
     * @return Breite des Spielfeldes.
     */
    public int getWidth() {
        return this.width;
    }

    /**
     * gibt die Höhe des Spielfeldes zurück.
     *
     * @return Höhe des Spielfeldes.
     */
    public int getHeigth() {
        return this.height;
    }

    /**
     * verändert den Status des Spielfeldes anhand der angegebenen Werte.
     *
     * @param r Zeilennummer des Feldes.
     * @param c Spaltennummer des Feldes.
     */
    public void changeState(int r, int c) {
        if (nextField[r][c] == true) {
            nextField[r][c] = false;
        } else if (nextField[r][c] == false) {
            nextField[r][c] = true;
        }
    }

    /**
     * leert das Spielfeld
     */
    public void clear() {
        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                nextField[i][j] = false;
                actualField[i][j] = false;
            }
        }

    }

    /**
     * gibt den Status bei der angegebenen Stelle des Feldes zurück.
     *
     * @param w die Zeilennummer des Feldes.
     * @param h die Spaltennummer des Feldes.
     * @return
     */
    public boolean isAlive(int w, int h) {
        return nextField[w][h];
    }

    /**
     * stellt die nächste Generation der Zellen bereit.
     */
    public void nextGeneration() {
        this.conwayRules();
        boolean[][] historyField = new boolean[this.width][this.height];

        for (int x = 0; x < this.width; x++) {
            for (int y = 0; y < this.height; y++) {
                historyField[x][y] = actualField[x][y];
            }
        }

        if (fieldHistory.size() >= 10) {
            fieldHistory.remove(fieldHistory.size() - 1);
        }
        fieldHistory.add(0, historyField);

        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                actualField[i][j] = nextField[i][j];
            }
        }

    }

    /**
     * stellt die vorherige Generation der Zellen bereit.
     */
    public void previousGeneration() {

        if (!fieldHistory.isEmpty()) {
            boolean prevField[][] = fieldHistory.get(0);

            for (int i = 0; i < this.width; i++) {
                for (int j = 0; j < this.height; j++) {
                    nextField[i][j] = prevField[i][j];
                }
            }

            fieldHistory.remove(0);
        }

    }

    /**
     * überprüft die Nachbarn einer Zelle und gibt zurück, wie viele leben.
     *
     * @param grid das Feld dessen Nachbarn gezählt werden sollen.
     * @param i die Zeilennummer der zu untersuchenden Zelle.
     * @param j die Spaltennummer der zu untersuchenden Zelle.
     * @return Anzahl lebender Nachbarn der Zelle.
     */
    public static int countNeighbours(boolean[][] grid, int i, int j) {
        int count = 0;
        int nextX = i + 1;
        int nextY = j + 1;
        int prevX = i - 1;
        int prevY = j - 1;

        if (i == 0) {
            prevX = grid.length - 1;

        }
        if (j == 0) {
            prevY = grid[i].length - 1;

        }

        if (i == grid.length - 1) {
            nextX = 0;

        }
        if (j == grid[i].length - 1) {
            nextY = 0;

        }

        if (grid[prevX][j] == true) {
            count++;
        }
        if (grid[nextX][j] == true) {
            count++;
        }
        if (grid[i][prevY] == true) {
            count++;
        }
        if (grid[i][nextY] == true) {
            count++;
        }
        if (grid[prevX][prevY] == true) {
            count++;
        }
        if (grid[nextX][nextY] == true) {
            count++;
        }
        if (grid[nextX][prevY] == true) {
            count++;
        }
        if (grid[prevX][nextY] == true) {
            count++;
        }
        return count;
    }

    /**
     * berechnet anhand der Conway-Regeln die nächste Generation.
     */
    private void conwayRules() {

        for (int i = 0; i < 50; i++) {
            for (int j = 0; j < 50; j++) {

                int count = 0;
                count = countNeighbours(actualField, i, j);
                if (actualField[i][j] == true) {

                    if (count < 2) {
                        nextField[i][j] = false;
                    }

                    if (count == 2 || count == 3) {
                        nextField[i][j] = true;
                    }
                    if (count > 3) {
                        nextField[i][j] = false;
                    }
                }
                if (actualField[i][j] == false) {
                    if (count == 3) {
                        nextField[i][j] = true;
                    }
                }

            }
        }

    }
}
