package GameOfLife;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class View {

    private JButton play;
    private JButton pause;
    private JButton back;
    private JButton clear;
    private JButton random;
    private JGoLField[][] field;

    private Controler myControler;
    private Model myModel;

    public List<JButton> buttons;

    /**
     * erstellt die grafische Oberfläche des Spiels.
     */
    public View() {
        buttons = new LinkedList<JButton>();
        this.myModel = new Model(50, 50);
        this.myControler = new Controler(this.myModel, this);
        play = new JButton("Play"); //0
        buttons.add(play);
        pause = new JButton("Pause");//1
        buttons.add(pause);
        back = new JButton("Back"); //2
        buttons.add(back);
        clear = new JButton("clear"); //3
        buttons.add(clear);
        random = new JButton("random"); //4
        buttons.add(random);
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());
        buttonPanel.add(play);
        buttonPanel.add(pause);
        buttonPanel.add(back);
        buttonPanel.add(clear);
        buttonPanel.add(random);
        for (JButton b : buttons) {
            b.addMouseListener(myControler);
        }

        JFrame mainFrame = new JFrame("Game of Life");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(1000, 800);

        field = new JGoLField[myModel.getWidth()][myModel.getHeigth()];
        JPanel gridPanel = new JPanel();
        gridPanel.setLayout(new GridLayout(myModel.getWidth(), myModel.getHeigth()));

        // hier werden die elemente nach y-x reihenfolge eingefügt, weil das Layout
        // die elemente dementsprechend einfügt.
        for (int i = 0; i < myModel.getWidth(); i++) {
            for (int j = 0; j < myModel.getHeigth(); j++) {
                field[j][i] = new JGoLField(j, i, false);
                field[j][i].addMouseListener(this.myControler);
                gridPanel.add(field[j][i]);
            }
        }

        mainFrame.setLayout(new BorderLayout());
        mainFrame.add(gridPanel, BorderLayout.CENTER);
        mainFrame.add(buttonPanel, BorderLayout.SOUTH);
        mainFrame.setVisible(true);

    }

    /**
     * aktualisiert den visuellen Zustand der View.
     */
    public void update() {
        for (int i = 0; i < myModel.getWidth(); i++) {
            for (int j = 0; j < myModel.getHeigth(); j++) {
                field[i][j].setAlive(myModel.isAlive(i, j));

                field[i][j].update();
            }

        }

    }

}
