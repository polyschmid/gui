package GameOfLife;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class JGoLField extends JPanel {

    private int row;
    private int col;
    private boolean alive;

    /**
     *
     * konstruiert ein JGoLField-Objekt.
     *
     * @param r die Reihennummer die das Objekt annehmen soll.
     * @param c die Spaltennummer die das Objekt annehmen soll.
     * @param status der Zustand (tot oder lebendig) den das Objekt annehmen
     * soll.
     */
    public JGoLField(int r, int c, boolean status) {
        this.row = r;
        this.col = c;
        this.alive = status;
    }

    /**
     * gibt die Reihennummer zurück.
     *
     * @return Reihennummer des Objekts
     */
    public int getRow() {
        return this.row;
    }

    /**
     * gibt die Spaltennummer zurück.
     *
     * @return Spaltennummer des Objekts.
     */
    public int getCol() {
        return this.col;
    }

    /**
     * setzt den Objektstatus auf tot(false) oder lebendig(true)
     *
     * @param status den Status den das Objekt annehmen soll.
     */
    public void setAlive(boolean status) {
        this.alive = status;
    }

    /**
     * gibt zurück ob das Objekt tot(false) oder lebendig(true) ist
     *
     * @return status des Objekts.
     */
    public boolean isAlive() {
        return this.alive;
    }

    /**
     * aktualisiert die Visuelle darstellung des Objekts.
     */
    public void update() {
        this.paintComponent(getGraphics());
    }

    /**
     * zeichnet das Objekt
     */
    public void paintComponent(Graphics g) {

        if (this.isAlive()) {
            g.setColor(Color.GRAY);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
        } else if (!this.isAlive()) {
            g.setColor(Color.LIGHT_GRAY);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
        }
        g.setColor(Color.WHITE);
        g.drawRect(0, 0, this.getWidth(), this.getHeight());
    }
}
