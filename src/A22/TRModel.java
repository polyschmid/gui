package A22;

/**
 *
 * @author Hannes
 */
public class TRModel {
    /**
     * Ergebniss
     */
    private String result = "";
    /**
     * Operator
     */
    private String operator = "";
    /**
     * Operand
     */
    private String operand = "";
    /**
     * Error - Fehler
     */
    private String error = "";
    
    /**
     * Konstruktor
     * Gibt das Ergebnis zurück.
     */
    public TRModel(){
        this.result = "";
    }
    /**
     * Berechnet die eingegebenen Werte des Taschenrechners.
     */
    public void berechne() {
        try {
            // Resultat, Operator oder Operand liegen nicht vor => tue nichts
            if (this.result.equals("") || this.operator.equals("") || this.operand.equals("")) {
                return;
            }
            // Ab hier normale Verarbeitung
            float a = Float.valueOf(this.result);
            float b = Float.valueOf(this.operand);
            if (this.operator.equals("+")) {
                this.result = String.valueOf(a + b);
            }
            if (this.operator.equals("-")) {
                this.result = String.valueOf(a - b);
            }
            if (this.operator.equals("/")) {
                // Nicht durch Null teilen
                if (b == 0.0) {
                    throw new Exception("Division by Zero");
                }
                this.result = String.valueOf(a / b);
            }
            if (this.operator.equals("*")) {
                this.result = String.valueOf(a * b);
            }
            this.operator = "";
            this.operand = "";
            this.error = "";
        } catch (Exception ex) {
            this.clear();
            this.error = ex.getMessage();
        }
    }
    /**
     * Getter Operand
     * @return String
     */
    public String getOperand(){
        return this.operand;
    }
    /**
     * Setter Operand
     * @param operand String 
     * @return String setzt den Operand.
     */
    public String setOperand(String operand){
        return this.operand = operand;
    }
    /**
     * Getter Operator
     * @return String
     */
    public String getOperator(){
        return this.operator;
    }
    /**
     * Setter für Operator
     * @param op String Setzt den Operator
     */
    public void setOperator(String op) {
    // Resultat, Operator und Operand existieren aus vorherigen Eingaben => erstmal
    // rechnen
        if (!(this.result.equals("") && this.operator.equals("") && this.operand.equals(""))) {
            this.berechne();
            if (!this.getError().equals("")) {
                return;
            }
        // Wenn Fehler aufgetreten, Methode verlassen
        }
        // Es wurde bereits ein Operand eingegeben => diesen zum Resultat machen
        if (!this.operand.equals("")) {
            this.result = this.operand;
            this.operand = "";
        }
        /*
        * Hier will er noch nicht richtig
        */
        // Es liegt kein Resultat bei + oder - vor => Resultat auf Null setzen
         // Es liegt kein Resultat bei * oder / vor => Resultat auf Null setzen
        if (this.result.equals("") && (op.equals("+")||op.equals("-"))) {
            this.result = "0";
        }      
        if (this.result.equals("") && (op.equals("*")||op.equals("/"))) {
            this.result = "1";
        }
        
        this.operator = op;
        this.error = "";
    }
    /**
     * Getter für den Error
     * @return String gibt den Fehler 
     */
    public String getError(){
        return this.error;
    }
    /**
     * Leert alle Werte des Taschenrechners.
     */
    public void clear(){
        this.result = "";
        this.operator = "";
        this.operand = "";
        this.error = "";
    }
    /**
     * Getter Result
     * @return String gibt das Ergebnis zurück.
     */
    public String getResult() {
        return this.result;
    }

}
