package A18_1bis4;

/**
 *
 * @author hannes
 */
public class Quadrat extends Rechteck {

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param a int Maße "a" des Objekt
     *
     */
    public Quadrat(int x, int y, int z, int a) {
        super(x, y, z, a, a);
    }
}
