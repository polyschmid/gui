package A18_1bis4;

/**
 *
 * @author hannes
 */
public class Kreis extends Ellipse {

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param r int Radius des Kreise
     */
    public Kreis(int x, int y, int z, int r) {

        super(x, y, z, r, r);
    }
}
